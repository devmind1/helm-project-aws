### infra

deploy infra with terraform
```
terraform init
terraform plan
terraform apply
```

destroy:
```
terraform init
terraform destroy -target module.demo_eks
terraform destroy
```