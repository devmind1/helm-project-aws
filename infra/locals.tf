locals {
  region = "eu-west-1"

  demo_vpc_name    = "demo-vpc"
  demo_eks_name    = "demo-eks"
  demo_eks_version = "1.21"
  demo_iam_name    = "demo-role"
  demo_docdb_name  = "demo-docdb-cluster"

  tags = {
    Envrionment = "Dev"
    Service     = "my-demo-service"
    Managed     = "Terraform"
  }
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}