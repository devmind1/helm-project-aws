terraform {
  required_version = "~> 1.1.9"

  required_providers {
    aws = "~> 4.0.0"
  }

  # uncomment to enable remote backend
  backend "s3" {
    bucket = "michael-helm"
    key    = "vpc-eks-apps.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = local.region
}