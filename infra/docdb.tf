# random password
resource "random_password" "password" {
  length  = 16
  special = false
}

# db subnet group
resource "aws_db_subnet_group" "demo_db_subnet_group" {
  name       = "demo-db-subnet-group"
  subnet_ids = module.demo_vpc.database_subnets
  tags = merge({
    Name = "demo-db-subnet-group"
  }, local.tags)
}

# db security group
resource "aws_security_group" "demo_db_sg" {
  name        = join("-", [local.demo_docdb_name, "sg"])
  description = "docdb sg"
  vpc_id      = module.demo_vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = [module.demo_vpc.vpc_cidr_block]
  }
}

# doc db cluster
resource "aws_docdb_cluster" "demo_docdb_cluster" {
  cluster_identifier      = local.demo_docdb_name
  engine                  = "docdb"
  engine_version          = "3.6.0"
  master_username         = "demouser"
  master_password         = random_password.password.result
  availability_zones      = ["${local.region}a", "${local.region}b", "${local.region}c"]
  db_subnet_group_name    = aws_db_subnet_group.demo_db_subnet_group.name
  vpc_security_group_ids  = [aws_security_group.demo_db_sg.id]
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  skip_final_snapshot     = true
  tags = merge({
    Name = local.demo_docdb_name
  }, local.tags)
}

# db instance
resource "aws_docdb_cluster_instance" "demo_docdb_instances" {
  count                      = 2 # number of db instances
  identifier                 = join("-", [local.demo_docdb_name, count.index])
  cluster_identifier         = aws_docdb_cluster.demo_docdb_cluster.id
  instance_class             = "db.r5.large"
  auto_minor_version_upgrade = true
  tags                       = local.tags
}