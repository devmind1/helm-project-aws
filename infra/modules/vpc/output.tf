output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "vpc id"
}

output "vpc_cidr_block" {
  value       = module.vpc.vpc_cidr_block
  description = "vpc cidr"
}

output "private_subnets" {
  value       = module.vpc.private_subnets
  description = "vpc private subnets"
}

output "public_subnets" {
  value       = module.vpc.public_subnets
  description = "vpc public subnets"
}

output "database_subnets" {
  value       = module.vpc.database_subnets
  description = "vpc database subnets"
}