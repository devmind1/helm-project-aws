
# demo eks cluster
module "demo_eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "v17.22.0"

  cluster_name    = local.demo_eks_name
  cluster_version = local.demo_eks_version

  vpc_id  = module.demo_vpc.vpc_id
  subnets = module.demo_vpc.public_subnets

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  #cluster_endpoint_public_access_cidrs = ["trusted_ip",...]

  enable_irsa      = true
  write_kubeconfig = false

  map_roles = [
    {
      rolearn  = aws_iam_role.demo_admin.arn
      username = aws_iam_role.demo_admin.name
      groups   = ["system:masters"]
    }
  ]

  #   map_users = [
  #     {
  #       userarn  = "arn:aws:iam::1234567890:user/<your_user_name>"
  #       username = "<your_user_name>"
  #       groups   = ["systems:masters"]
  #     }
  #   ]

  node_groups_defaults = {
    ami_type   = "AL2_x86_64"
    disk_size  = 50
    k8s_labels = {}
    additional_tags = merge(local.tags, {
      nodegroup-role = "worker"
    })
    create_launch_template = true
  }

  node_groups = {
    mng_121 = {
      desired_capacity = 1
      max_capacity     = 3
      min_capacity     = 1

      instance_types = ["m4.large"]
      subnets        = module.demo_vpc.private_subnets
      k8s_labels     = {}
      additional_tags = {
        Name = "mng_121"
      }
      update_config = {
        max_unavailable_percentage = 50
      }
    }
  }
  tags = merge(local.tags, {
    Name = local.demo_eks_name
  })
}

data "aws_eks_cluster" "demo_eks" {
  name = module.demo_eks.cluster_id
}

data "aws_eks_cluster_auth" "demo_eks" {
  name = module.demo_eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.demo_eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.demo_eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.demo_eks.token
}