module "demo_vpc" {
  source           = "./modules/vpc"
  name             = local.demo_vpc_name
  vpc_cidr         = "10.10.0.0/16"
  azs              = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets  = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  public_subnets   = ["10.10.11.0/24", "10.10.12.0/24", "10.10.13.0/24"]
  database_subnets = ["10.10.21.0/24", "10.10.22.0/24", "10.10.23.0/24"]
  tags = merge({
    "kubernetes.io/cluster/${local.demo_eks_name}" = "shared"
  }, local.tags)
}