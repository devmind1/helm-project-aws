### alb ingress controller
deploy using kustomize

```
kustomize build demo/ | kubectl apply -f -
```

If using latest versions of `kubectl` then use kustomize flag of kubectl
```
kubectl apply -k demo/
```

_Note: Make sure to replace `<youraccountid>` in the [patch](./demo/kustomization.yaml) with your acctual aws account id._