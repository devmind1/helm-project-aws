### k8s apps
install each app using helm. follow below commands:

postgres:
```
helm install -f kanban-postgres.yaml postgres ./postgres
helm list
kubectl get deployments
```

apps:
```
helm install -f adminer.yaml adminer ./app
helm install -f kanban-app.yaml kanban-app ./app
helm install -f kanban-ui.yaml kanban-ui ./app

helm list 
kubectl get deployments
```

ingress:
```
kubectl apply -f _ingress.yaml
```